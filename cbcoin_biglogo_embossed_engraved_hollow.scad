$fn=100;

        rotate([0,0,52.5])translate([-8.5,3.4,0])linear_extrude(2.33)scale(0.30)text("CODE",font="monospace:BOLD");
   
difference() 
{
   
    cylinder(h=2.33,d=23.25);
    {
        translate([-30.5,-22,-0.01])linear_extrude(2.35)scale(1.75)import("codeberg_logo.svg");

        translate([-30.5,-22,1.9])linear_extrude(0.8)scale(1.75)import("codeberg_logo_only_pac.svg");  

        rotate([0,0,52.5])translate([-8.5,-0.0,1.7])linear_extrude(0.8)scale(0.30)text("BERG",font="monospace:BOLD");   
    }
}


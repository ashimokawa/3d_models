$fn=100;

        rotate([0,0,45])translate([-5.5,4.5,1.2])linear_extrude(1)scale(0.30)text("CODE",font="monospace:BOLD");
        rotate([0,0,-45])translate([-4,4.5,1.2])linear_extrude(1)scale(0.30)text("BERG",font="monospace:BOLD");
   
difference() 
{
   
    cylinder(h=2.33,d=23.25);
    {
        translate([-30.5,-22,1.5])linear_extrude(1)scale(1.75)import("codeberg_logo.svg");

        translate([-30.5,-22,1.9])linear_extrude(0.8)scale(1.75)import("codeberg_logo_only_pac.svg");  

      //rotate([0,0,0])translate([-8,-7,2])linear_extrude(1)scale(0.30)text(".ORG",font="monospace:BOLD");   
    }
}

